//
//  MessangerViewController.swift
//  MoMentor
//
//  Created by Filbert Hartawan on 28/03/19.
//  Copyright © 2019 Pancuan. All rights reserved.
//

import UIKit

class MessangerViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var navigationBar: UINavigationItem!
    
    private let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isTranslucent = false
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.height, height: view.frame.height))
        titleLabel.text = "Messenger"
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        titleLabel.font = UIFont.systemFontSize
        navigationItem.titleView = titleLabel
        collectionView?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(FriendCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        collectionView.scrollIndicatorInsets = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        
        setupMenuBar()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 3
        }
    
        override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            return collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        }
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
            return CGSize(width: view.frame.width, height: 100)
        }
    
    let menuBar: MenuBar = {
        let mb = MenuBar()
        return mb
    }()
    
    func setupMenuBar(){
        view.addSubview(menuBar)
        view.addConstaintsWithFormat(format: "H:|[v0]|", align: "h", views: menuBar)
        view.addConstaintsWithFormat(format: "V:|[v0(50)]", align: "v", views: menuBar)
    }
}

class MenuBar: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.7529411765, blue: 0.7960784314, alpha: 1)
        cv.dataSource = self
        cv.delegate  = self
        
        return cv
    }()
    
    let cellId = "cellId"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        addConstaintsWithFormat(format: "H:|[v0]|", align: "h", views: collectionView)
        addConstaintsWithFormat(format: "V:|[v0(50)]", align: "v", views: collectionView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width : frame.width/2, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    class MenuCell: BaseCell {
        
        let iconImageView: UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "messageIcon")
            imageView.contentMode = .scaleAspectFill
            return imageView
        }()

        
        override func setupViews() {
            super.setupViews()
            addSubview(iconImageView)
            addConstaintsWithFormat(format: "H:|[v0(28)]|", align: "h", views: iconImageView)
            addConstaintsWithFormat(format: "V:|[v0(28)]|", align: "v", views: iconImageView)
        }
        
    }

}



class FriendCell: BaseCell {
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 34
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let dividerLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        return view
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Atta Halilintar"
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = #colorLiteral(red: 0.1490196078, green: 0.1490196078, blue: 0.1490196078, alpha: 1)
        return label
    }()
    
    let messegeLabel: UILabel = {
        let label = UILabel()
        label.text = "Friend Messege is here hahahahahahahaha !"
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = #colorLiteral(red: 0.568627451, green: 0.5882352941, blue: 0.6392156863, alpha: 1)
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.text = "01.00 AM"
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = #colorLiteral(red: 0.568627451, green: 0.5882352941, blue: 0.6392156863, alpha: 1)
        return label
    }()
    
    
    override func setupViews() {
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        addSubview(profileImageView)
        addSubview(dividerLineView)
        
        setupContainerView()
        
        profileImageView.image = UIImage(named:"atta")
        
        addConstaintsWithFormat(format: "H:|-12-[v0(68)]", align: "h",views: profileImageView)
        addConstaintsWithFormat(format: "V:[v0(68)]" ,align: "v",views: profileImageView)
        
        addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        addConstaintsWithFormat(format: "H:|-82-[v0]|", align: "h",views: dividerLineView)
        addConstaintsWithFormat(format: "V:[v0(1)]|" ,align: "v",views: dividerLineView)
    }
    
    private func setupContainerView(){
        let containerView = UIView()
        addSubview(containerView)
        addConstaintsWithFormat(format: "H:|-90-[v0]|", align: "h",views: containerView)
        addConstaintsWithFormat(format: "V:[v0(50)]" ,align: "v",views: containerView)
        addConstraint(NSLayoutConstraint(item: containerView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        containerView.addSubview(nameLabel)
        containerView.addSubview(timeLabel)
        containerView.addSubview(messegeLabel)
        containerView.addConstaintsWithFormat(format: "H:|[v0][v1(80)]|", align: "h", views: nameLabel, timeLabel)
        containerView.addConstaintsWithFormat(format: "V:|[v0][v1(24)]|", align: "v" ,views: nameLabel, messegeLabel)
        containerView.addConstaintsWithFormat(format: "H:|[v0]-8-|", align: "h",views: messegeLabel)
        
        containerView.addConstaintsWithFormat(format: "V:|[v0(20)]|", align: "v", views: timeLabel)
    }
}

extension UIView{
    func addConstaintsWithFormat(format: String,align:String, views: UIView...){
        
        var viewDictionary = [String:UIView]()
        
        for(index, view) in views.enumerated(){
            let key = "v\(index)"
            viewDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        if(align=="v"){
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: viewDictionary))
        }else{
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions.alignAllCenterY, metrics: nil, views: viewDictionary))
        }
        
        
    }
}

class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        backgroundColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
    }
    
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


