//
//  CustomNavigationViewController.swift
//  MoMentor
//
//  Created by Filbert Hartawan on 28/03/19.
//  Copyright © 2019 Pancuan. All rights reserved.
//

import UIKit

class CustomNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = #colorLiteral(red: 0.1764705882, green: 0.7058823529, blue: 0.7490196078, alpha: 1)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
