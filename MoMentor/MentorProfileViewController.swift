//
//  MentorProfileViewController.swift
//  MoMentor
//
//  Created by Hendry Husada on 29/03/19.
//  Copyright © 2019 Pancuan. All rights reserved.
//

import UIKit

class MentorProfileViewController: UIViewController {
    @IBOutlet weak var viewPost: UIView!
    @IBOutlet weak var viewProfile: UIView!
    
    
    
    @IBOutlet weak var mentorPhoto: UIImageView!
    @IBOutlet weak var SubsButton: UIButton!
    @IBOutlet weak var ConnectButton: UIButton!
    
    
    @IBAction func `switch`(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex==0{
            viewProfile.alpha=1
            viewPost.alpha=0
            
        }else{
            viewProfile.alpha=0
            viewPost.alpha=1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mentorPhoto.layer.cornerRadius=50
        mentorPhoto.layer.masksToBounds=true
        
        SubsButton.layer.cornerRadius=10
        SubsButton.clipsToBounds=true
        
        SubsButton.layer.borderWidth=2
        SubsButton.layer.borderColor=#colorLiteral(red: 0.02907937765, green: 0.7213051915, blue: 0.7301885486, alpha: 1)
        
        ConnectButton.layer.borderWidth=2
        ConnectButton.layer.borderColor=#colorLiteral(red: 0.02907937765, green: 0.7213051915, blue: 0.7301885486, alpha: 1)
    
        ConnectButton.layer.cornerRadius=10
        ConnectButton.clipsToBounds=true
        
    

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
